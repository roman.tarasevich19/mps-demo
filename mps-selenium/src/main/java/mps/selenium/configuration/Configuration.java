package mps.selenium.configuration;

public class Configuration {

    public static final String START_URL = "/startUrl";

    private Configuration() {
    }

    public static String getUrl(String nameValue) {
        return Environment.getCurrentEnvironment().getValue(nameValue).toString();
    }
}
