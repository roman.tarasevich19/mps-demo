package mps.selenium.pageobject.pages;

import mps.selenium.pageobject.forms.BaseForm;
import org.openqa.selenium.By;

public class HomePage extends BaseForm {

    public HomePage() {
        super(By.cssSelector("div[class='MenuLandingPageFeature']"), "Home page");
    }


}
