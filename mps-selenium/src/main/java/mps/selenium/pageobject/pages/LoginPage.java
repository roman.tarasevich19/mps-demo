package mps.selenium.pageobject.pages;

import aquality.selenium.elements.interfaces.IButton;
import aquality.selenium.elements.interfaces.ITextBox;
import mps.selenium.models.User;
import mps.selenium.pageobject.forms.BaseForm;
import org.openqa.selenium.By;

import java.util.concurrent.TimeUnit;

public class LoginPage extends BaseForm {

    private final ITextBox usernameTxb = getElementFactory().getTextBox(By.id("username"), "Username");
    private final ITextBox passwordTxb = getElementFactory().getTextBox(By.id("password"), "Password");
    private final IButton loginBtn = getElementFactory().getButton(By.id("kc-login"), "Login button");

    public LoginPage() {
        super(By.id("kc-content"), "Login page");
    }

    public void logInAs(User user) {
        usernameTxb.clearAndType(user.getEmail());
        passwordTxb.clearAndType(user.getPassword());
        clickLoginButton();
    }

    public void setUsername(String name){
        usernameTxb.clearAndType(name);
    }

    public void setPassword(String name){
        passwordTxb.clearAndType(name);
    }

    public void clickLoginButton() {
        loginBtn.clickAndWait();
        try {
            TimeUnit.SECONDS.sleep(45);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}