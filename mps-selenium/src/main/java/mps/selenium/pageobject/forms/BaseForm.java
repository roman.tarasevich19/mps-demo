package mps.selenium.pageobject.forms;

import aquality.selenium.elements.interfaces.*;
import aquality.selenium.forms.Form;
import org.openqa.selenium.By;

public abstract class BaseForm extends Form {

    private static final String ALERT_MESSAGE_TEMPLATE = "//*[contains(text(),\"%s\")]";

    protected BaseForm(By locator, String name) {
        super(locator, name);
    }

    public boolean isAlertMessageDisplayed(String message) {
        ILabel alertLabel = getElementFactory().getLabel(
                By.xpath(String.format(ALERT_MESSAGE_TEMPLATE, message)), message);
        return alertLabel.state().isDisplayed();
    }
}
