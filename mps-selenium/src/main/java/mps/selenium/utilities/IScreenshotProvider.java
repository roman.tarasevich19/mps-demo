package mps.selenium.utilities;

public interface IScreenshotProvider {

    byte[] takeScreenshot();
}
