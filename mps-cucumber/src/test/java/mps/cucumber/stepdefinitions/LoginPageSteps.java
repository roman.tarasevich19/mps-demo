package mps.cucumber.stepdefinitions;

import mps.cucumber.utilities.ScenarioContext;
import mps.selenium.models.User;
import mps.selenium.pageobject.pages.LoginPage;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;

import javax.inject.Inject;

public class LoginPageSteps {

    private final LoginPage loginPage;
    private final ScenarioContext scenarioContext;

    @Inject
    public LoginPageSteps(ScenarioContext scenarioContext) {
        this.scenarioContext = scenarioContext;
        loginPage = new LoginPage();
    }

    @Then("Login page is opened")
    public void loginPageIsOpened() {
        Assert.assertTrue(loginPage.isDisplayed(), "Login page should be opened");
    }

    @When("I login as {string}")
    public void login(String key){
        User user = scenarioContext.get(key);
        loginPage.logInAs(user);
    }

    @When("I set username {string}")
    public void setUsername(String username){
        loginPage.setUsername(username);
    }

    @When("I set password {string}")
    public void setPassword(String username){
        loginPage.setPassword(username);
    }

    @When("I click login")
    public void login(){
        loginPage.clickLoginButton();
    }

}
