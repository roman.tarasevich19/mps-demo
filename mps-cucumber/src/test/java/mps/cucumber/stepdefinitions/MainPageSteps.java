package mps.cucumber.stepdefinitions;

import mps.cucumber.utilities.ScenarioContext;
import mps.selenium.pageobject.pages.HomePage;
import io.cucumber.java.en.Then;
import org.testng.Assert;

import javax.inject.Inject;

public class MainPageSteps {

    private final HomePage homePage;
    private final ScenarioContext scenarioContext;

    @Inject
    public MainPageSteps(ScenarioContext scenarioContext) {
        this.scenarioContext = scenarioContext;
        homePage = new HomePage();
    }

    @Then("Home page is opened")
    public void loginPageIsOpened() {
        Assert.assertTrue(homePage.isDisplayed(), "Home page should be opened");
    }
}
