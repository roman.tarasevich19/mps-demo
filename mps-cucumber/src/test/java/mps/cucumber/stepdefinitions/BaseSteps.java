package mps.cucumber.stepdefinitions;

import io.cucumber.java.en.Then;
import mps.cucumber.utilities.ScenarioContext;
import mps.selenium.pageobject.pages.HomePage;
import org.testng.Assert;

import javax.inject.Inject;

public class BaseSteps {

    private final ScenarioContext scenarioContext;
    private final HomePage homePage;

    @Inject
    public BaseSteps(ScenarioContext scenarioContext) {
        this.scenarioContext = scenarioContext;
        this.homePage = new HomePage();
    }

    @Then("Alert message {string} is displayed")
    public void alertMessageIsDisplayed(String message){
        Assert.assertTrue(homePage.isAlertMessageDisplayed(message), String.format("Message %s should be present", message));
    }

    @Then("Alert message {string} is not displayed")
    public void alertMessageIsNotDisplayed(String message){
        Assert.assertFalse(homePage.isAlertMessageDisplayed(message), String.format("Message %s should be not present", message));
    }
}
