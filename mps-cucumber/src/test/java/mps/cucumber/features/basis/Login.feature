Feature: Sachkonto

  Background:
    Given Login page is opened

  @Basis
  Scenario: MPSQG:Basis 0001 Login without required fields
    Given Alert message 'Ungültiger Benutzername oder Passwort.' is not displayed
    When I click login
      Then Alert message 'Ungültiger Benutzernam oder Passwort.' is displayed

  @Basis
  Scenario: MPSQG:Basis 0002 Login with invalid username and password
    When I set username 'someUsername'
    And I set password 'somePassword'
    And I click login
      Then Alert message 'Ungültiger Benutzernam oder Passwort.' is displayed
