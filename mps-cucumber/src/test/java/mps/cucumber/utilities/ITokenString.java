package mps.cucumber.utilities;

public interface ITokenString {
    String toString();
}
