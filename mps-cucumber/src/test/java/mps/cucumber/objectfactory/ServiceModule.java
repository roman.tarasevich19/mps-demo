package mps.cucumber.objectfactory;

import mps.selenium.utilities.IScreenshotProvider;
import mps.selenium.utilities.ScreenshotProvider;
import com.google.inject.AbstractModule;

final class ServiceModule extends AbstractModule {

    @Override
    protected void configure() {
        this.bind(IScreenshotProvider.class).toInstance(new ScreenshotProvider());
    }
}
