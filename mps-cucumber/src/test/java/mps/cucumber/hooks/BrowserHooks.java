package mps.cucumber.hooks;

import aquality.selenium.browser.AqualityServices;
import io.cucumber.java.After;
import io.cucumber.java.Before;

public class BrowserHooks {

    @Before(order = 0)
    public void goToStartUrl() {
        AqualityServices.getBrowser().goTo("https://Roman.Tarasevich:Nhjkjkjuu!1994@lab.mps-solutions.de/demo/3/react/oauth/login.html");
    }

    @After(order = 0)
    public void closeBrowser() {
        if (AqualityServices.isBrowserStarted()) {
            AqualityServices.getBrowser().quit();
        }
    }
}
