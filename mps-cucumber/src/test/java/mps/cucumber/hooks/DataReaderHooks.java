package mps.cucumber.hooks;

import mps.cucumber.utilities.ScenarioContext;
import mps.selenium.models.User;
import io.cucumber.java.Before;

import javax.inject.Inject;

public class DataReaderHooks {

    private final ScenarioContext scenarioContext;

    @Inject
    public DataReaderHooks(ScenarioContext scenarioContext) {
        this.scenarioContext = scenarioContext;
    }

    @Before(order = 0)
    public void getUserInfo() {
        User user = new User();
        user.setEmail(System.getProperty("E2E_USER"));
        user.setPassword(System.getProperty("E2E_PASSWORD"));
        scenarioContext.add("user", user);
    }
}
